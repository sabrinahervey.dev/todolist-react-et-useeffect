import React, { useState } from "react";
import "./App.css";
import TodoList from "./components/TodoList";
//import d'axios dans le fichier App.jsx
import axios from "axios";

//liste des taches contenu dans l'url de la constante API
const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  //on utilise useEffect pour faire une requete 
  // la fonction sera exécuté 1 seule fois
  React.useEffect(() => {
    axios.get(API_URL).then((Response) => {
      setTodos(Response.data);
    });
  }, []);

//on retourne le résultat
  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
